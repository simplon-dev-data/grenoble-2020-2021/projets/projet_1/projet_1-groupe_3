#!/usr/bin/env python3
# -*- coding: utf8 -*-
# accesslog2csv: Convert default, unified access log from Apache, Nginx
# servers to CSV format.

import csv
import re
import sys
from device_detector import DeviceDetector #Librairie d'analyse du useragaent


#Controle utilisation du scripts
if len(sys.argv) == 1:
    sys.stdout.write("Usage: %s <access.log> <accesslog.csv>\n"%sys.argv[0])
    sys.exit(0)

log_file_name = sys.argv[1]
csv_file_name = sys.argv[2]

#Regex
pattern = re.compile(r'(?P<host>\S+).(?P<rfc1413ident>\S+).(?P<user>\S+).\[(?P<datetime>\S+ \+[0-9]{4})]."(?P<httpverb>\S+) (?P<url>\S+) (?P<httpver>\S+)" (?P<status>[0-9]+) (?P<size>\S+) "(?P<referer>[^"]*)" "(?P<useragent>[^"]*)"\s*')

file = open(log_file_name)
count_error = 0

#Ouverture du fichier CSV
with open(csv_file_name, 'w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['host', 'ident', 'user', 'time', 'verb', 'url', 'httpver', 'status', 'size', 'referer', 'useragent', 'os', 'client', 'device_type'])

#Allimentation du fichier CSV avec le fichier log selon la regex definit plus haut

    for line in file:
        if pattern.match(line):
            m = pattern.match(line)
            result = m.groups()
            results = list(result)
            ua = m.group(11)
            device = DeviceDetector(ua).parse()
            os = device.os_name()
            client = device.client_name()
            device_type = device.device_type()
            results.append(os)
            results.append(client)
            results.append(device_type)
            csv_out.writerow(results)
        else:
            count_error += 1
print (count_error)	        