# Projet 1 - Groupe 3 Log site e-commerce

Membres du groupe 3 : François (Okolo), Merouane

[Lien slides](https://docs.google.com/presentation/d/11jKcH95kA2pSj87vwG7Z-zKyA-SHlimfbpc2U_FZEs4/edit?usp=sharing)

## Contexte
 Nous avons récupéré les logs d'un serveur d'un site-web de e-commerce. Nous souhaitons pouvoir effectuer une mesure d'audience des visiteurs.
 
 
 Le fichier recupérer est un fichier au format Common Log Format. 

 Grace à ce fichier nous devons essayer de repondre en groupe au question suivante sous forme d'un dashboard.


- Quel est le traffic sur le site ?
- Combien de personnes visitent le site ?
- Quels sont nos produits les plus et moins intéressants pour notre clientèle ?
- De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site ?
- Quels moments de la journée sont les plus/moins propices à la vente ?
- Quels navigateurs sont les plus / moins utilisés par les visiteurs ?
- Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs ?
- Quels appareils sont les plus / moins utilisés par les visiteurs ?


# Partie I : Collecte des données

Comme nous l'avons dit les données proviennent d'un serveur web NGINX et sont contenues dans un fichier au format log de 3.4 GB.

Le log que nous avons représente une semaine de connexion au serveur et contient environ 10.3 M d'enregistrement. 


# Partie II : Transformation des données

Notre but ici est d'intégrer les données du log dans une base de donnée SQLite. La problématique est, que SQLite ne sais pas comment intégrer des donnée au format log.
Il nous faut donc transformer ce fichier log en un fichier que SQLite sera intégrer en base de données.


Pour cela j'ai fait quelques recherches sur Internet afin de trouver un script en python afin de transformer un fichier Common Log Format en fichier csv.

Ces deux formats étant très courant je n'ai pas eu de mal à trouver un script fonctionnel.
Cependant, le script ne faisait pas la conversion exactement comme je le voulais. En effet, j'ai dû adapter la regex du script afin de pouvoir sélectionner chaque partie du log format comme il faut ensuite de l'intégrer au fichier csv.

Finalement une fois le script modifié, pour l'utiliser, je dois lui passé en paramètre mon fichier au format log et le nom de mon fichier au format csv.
La convertion se fait ensuite de manière automatique pendant quelques minutes à cause du nombre d'enregistrements.

# Partie III : Intégration en base de donnée SQLite

Nous avons donc ici notre fichier de log au format csv. Nous pouvons désormais l'intégrer dans une base de donnée comme ceci :

```sql
.separator ","
.header on
.import accesslog.csv raw_data
```
On verifie ensuite que l'imortation c'est bien faite.

```sql
.tables
SELECT COUNT(*) FROM raw_data;
SELECT * FROM raw_data;
SELECT host FROM raw_data;
```
Si ces commandes ne renvoient pas d'erreurs et fonctionnent correctement l'intégration, c'est bien faite.

Notre unique table ici contient toutes les données y compris les données dont nous n'avons pas besoin. L'étape suivante est donc de transformer les données.
## Creation de la table de données propre.

Nous avons besoin seulement de ces données pour répondre aux premières questions : Host, Time, Url, Referer et Useragent.
On crée donc une table seulement avec ces données.

```sql
CREATE TABLE clear_log (host INT, time TEXT, url TEXT, referer TEXT, useragent TEXT);
```
Puis nous l'alimentons avec les données de notre première table (raw_data).
```sql
INSERT INTO clear_log SELECT host, time, url, referer, useragent FROM raw data;
```
Pour finir, on vérifie si la table a bien été créée et contient bien les bonnes données.

```sql
.tables
SELECT COUNT(*) FROM clear_log;
SELECT * FROM clear_log;
```

# Partie IV : Exploitation/Visualisation sous Metabase

Nos données ici on été collecter, transformer et intégrer sous forme d'une base de données, il nous reste maintenant à les exploiter et les visualiser. C'est ce que nous allons faire en important notre base de données sous Metabase.

On commence déjà par copier notre base de donnée dans le conteneur et le fichier de metabase, avec la commande suivante :
```bash
sudo cp /home/merouane/projet1_groupe3/access.db /home/merouane/metabase-decouverte/.docker/meatbase/access.db
```
On lance metabase ensuite sur notre navigateur puis on importe notre base depuis le panneau d'administration.

Notre base est maintenant importée avec nos deux tables de données.

## Requêtes SQL pour repondre au questions

Traffic du site internet :

```sql
SELECT COUNT(*) AS NOMBRE FROM clear_log;
```

Nombre de visiteurs :
```sql
SELECT COUNT(DISTINCT host) FROM clear_log;
```

Quels sont nos produits les plus et moins intéressants pour notre clientèle 

```sql
SELECT url, count(*) as nombre
FROM clear_log
WHERE url LIKE "/product/%"
GROUP BY url
ORDER BY COUNT(url) DESC
```
De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site 

```sql
SELECT referer, count(*)
FROM clear_log
where referer !="-"
GROUP BY referer;
```
On intègre ensuite sur notre dashboard les informations.

Pour répondre aux questions : 
 
- Quels navigateurs sont les plus / moins utilisés par les visiteurs ?
- Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs ?
- Quels appareils sont les plus / moins utilisés par les visiteurs ?

J'ai dû utiliser une libraire python pour pouvoir interpréter le "useragent" de notre log et extraire ces 3 informations.

La librairie python s'appelle DeviceDetector.

J'ai ensuite adapté le script pour avoir 3 nouvelles colonnes (Os, Navigateur, Device_type).

Les requêtes pour les questions sont donc :


Quels navigateurs sont les plus / moins utilisés par les visiteurs 

```sql
SELECT client, count(*)
from clear_log
group by client;
```
Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs 
```sql
SELECT os, count(*)
from clear_log
group by os;
```
Quels appareils sont les plus / moins utilisés par les visiteurs 
```sql
select device_type, count(*)
from clear_log
group by device_type;
```
 On intègre ensuite les résultat de toutes ces requêtes a notre tableau de bord final.

# Partie V : Automatisation de toute le pipeline

Ici, nous allons automatiser toutes nos parties précédentes dans un seul scripts python qui fera tout seul, du téléchargement du log jusqu'au chargement dans metabase et a la création de table.

