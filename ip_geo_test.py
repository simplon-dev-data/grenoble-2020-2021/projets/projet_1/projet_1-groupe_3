#!/usr/bin/env python3
# -*- coding: utf8 -*-


import requests
import re
import csv

requete = requests.get("https://api.hostip.info/get_html.php?ip=66.249.66.194&position=true")
data = requete.content
print (data)

pattern = re.compile(r'(?P<latitude>Latitude: [^\\]+)([^n].)(?P<longitude>Longitude: [\-0-9.]*)')

m = pattern.match(str(data))
result = m.groups()

print(result)