#!/usr/bin/env python3
# -*- coding: utf8 -*-

import urllib.request
from zipfile import ZipFile
import csv
import re
import sys
from device_detector import DeviceDetector #Librairie d'analyse du useragaent
import sqlite3
import subprocess
import os

#Controle utilisation du scripts
if len(sys.argv) == 1:
    sys.stdout.write("Usage: %s <access.log> <accesslog.csv> <name_database.db>\n"%sys.argv[0])
    sys.exit(0)


# ------------TÉLÉCHARGEMENT DU FICHER LOG ------------
print("téléchargement du fichier log...")

urllib.request.urlretrieve('https://dvn-cloud.s3.amazonaws.com/10.7910/DVN/3QBYB5/1688c5a03d4-1fa8cbd4cbd9?response-content-disposition=attachment%3B%20filename%2A%3DUTF-8%27%27Access.log.zip&response-content-type=application%2Fzip&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20201216T160523Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Credential=AKIAIEJ3NV7UYCSRJC7A%2F20201216%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=12cdbdc46213d94c386f12b36d4675db9f7a442917a22e3c92619385a3af3069', 'accesslog.zip')


# ------------DEZIPAGE DU FICHIER LOG ------------

file = "accesslog.zip"

# ouvrir le fichier zip en mode lecture
with ZipFile(file, 'r') as zip: 
    
    # extraire tous les fichiers
    print('extraction...') 
    zip.extractall() 
    print('Terminé!')

# ------------CONVERSION DU LOG EN CSV ------------
print ("conversion du log au format csv....")


log_file_name = sys.argv[1]
csv_file_name = sys.argv[2]
database_name = sys.argv[3]

#Regex
pattern = re.compile(r'(?P<host>\S+).(?P<rfc1413ident>\S+).(?P<user>\S+).\[(?P<datetime>\S+ \+[0-9]{4})]."(?P<httpverb>\S+) (?P<url>\S+) (?P<httpver>\S+)" (?P<status>[0-9]+) (?P<size>\S+) "(?P<referer>[^"]*)" "(?P<useragent>[^"]*)"\s*')

file = open(log_file_name)
count_error = 0

#Ouverture du fichier CSV
with open(csv_file_name, 'w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['host', 'ident', 'user', 'time', 'verb', 'url', 'httpver', 'status', 'size', 'referer', 'useragent', 'os', 'client', 'device_type'])

#Allimentation du fichier CSV avec le fichier log selon la regex definit plus haut

    for line in file:
        if pattern.match(line):
            m = pattern.match(line)
            result = m.groups()
            results = list(result)
            ua = m.group(11)
            device = DeviceDetector(ua).parse()
            os = device.os_name()
            client = device.client_name()
            device_type = device.device_type()
            results.append(os)
            results.append(client)
            results.append(device_type)
            csv_out.writerow(results)
        else:
            count_error += 1
print ("nombre de ligne qui ne match pas :", count_error)


# ------------CHARGEMENT EN BASE DE DONNÉE SQLITE ------------
print("chargement en base SQLite cours....")

subprocess.call(["sqlite3", database_name,
  ".separator ','",
  ".header on", 
  f".import {csv_file_name} raw_data"])

conn = sqlite3.connect(database_name)
c = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS clear_log (host INT, time TEXT, url TEXT, referer TEXT, os TEXT, client TEXT, device_type TEXT)")
c.execute("DELETE FROM clear_log")
c.execute("INSERT INTO clear_log SELECT host, time, url, referer, os, client, device_type FROM raw_data")
conn.commit()
conn.close()

print("TERMINE !!")